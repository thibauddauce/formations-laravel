module.exports = {
    content: ["./public/**/*.{html,js}"],
    theme: {
      extend: {
          colors: {
            'laravel': '#f4645f',
            'symfony': '#18171b',

          }
      },
    },
    plugins: [],
  }
  